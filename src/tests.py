import unittest
from model import ImageModel, UNKNOWN

class ImageModelTest(unittest.TestCase):
    def test_fill_with_class(self):
        #given
        model = ImageModel()
        symbols_dict = {0: [[0.1, 0.2], [0.3, 0.4]],
                        1: [[0.5]],
                        2: []}

        #when
        with_classes = model.fill_with_class(symbols_dict, UNKNOWN)

        #then
        self.assertDictEqual(with_classes, {0: [([0.1, 0.2], UNKNOWN), ([0.3, 0.4], UNKNOWN)],
                                            1: [([0.5], UNKNOWN)],
                                            2: []})
        
