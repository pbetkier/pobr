import collections
import itertools
from PySide.QtCore import Signal, Slot, QObject
from PySide.QtGui import QImage, QPixmap
import cv2 as cv
import numpy as np
import colorsys

UNKNOWN, CLUBS, DIAMONDS, HEARTS, SPADES = range(5)


class ImageModel(QObject):
    image_changed = Signal()

    def __init__(self):
        QObject.__init__(self)
        self.image = None
        self.bottoms = None
        self.color_for_class = {UNKNOWN: (0, 0, 255),
                                CLUBS: (0, 128, 255),
                                DIAMONDS: (255, 255, 0),
                                HEARTS: (0, 255, 255),
                                SPADES: (128, 0, 128)}

    def load_image(self, filename):
        self.image = cv.imread(filename)
        self.original_image = cv.imread(filename)
        self.image_changed.emit()

    def write_file(self, filename):
        cv.imwrite(filename, self.image)

    def current_pixmap(self):
        tmp_img = cv.cvtColor(self.image, cv.COLOR_BGR2RGB)
        array = np.asarray(tmp_img)
        array = array.astype(np.int32)

        self.array_for_image = (255 << 24) | (array[:, :, 0] << 16) | (array[:, :, 1] << 8) | (array[:, :, 2])
        if np.version.version == '1.7.1':
            self.array_for_image = self.array_for_image.astype(np.int32)
        image = QImage(self.array_for_image.data, tmp_img.shape[1], tmp_img.shape[0], QImage.Format_RGB32)
        return QPixmap.fromImage(image)

    @Slot()
    def binarize(self):
        tmp_gray = cv.cvtColor(self.image, cv.COLOR_BGR2GRAY)
        cv.adaptiveThreshold(tmp_gray, 255, cv.ADAPTIVE_THRESH_MEAN_C, cv.THRESH_BINARY, 9, 5, tmp_gray)
        self.image = cv.cvtColor(tmp_gray, cv.COLOR_GRAY2BGR)
        self.image_changed.emit()

    @Slot()
    def blur(self):
        tmp_gray = cv.cvtColor(self.image, cv.COLOR_BGR2GRAY)
        tmp_gray = cv.medianBlur(tmp_gray, 5)
        self.image = cv.cvtColor(tmp_gray, cv.COLOR_GRAY2BGR)
        self.image_changed.emit()

    def colors_generator(self):
        color_step = 50
        for r in range(0, 256, 3 * color_step):
            for g in range(0, 256, 2 * color_step):
                for b in range(256, 50, -color_step):
                    yield (b, g, r)

    def contour_middle(self, contour):
        """
        Computes the middle point of the given contour.
        :param contour: the contour to check.
        :return: the middle point.
        """
        rect = cv.boundingRect(contour)
        return rect[0] + rect[2] / 2, rect[1] + rect[3] / 2

    def distribute_symbols(self, to_distribute):
        """
        Distributes the given symbols among the bottoms.
        :param to_distribute: the symbols to distribute.
        :return: a dict of bottom ids to lists of corresponding symbols
        """
        symbols = collections.defaultdict(list)
        for symbol in to_distribute:
            mid = self.contour_middle(symbol)
            possible_bottoms = []
            for i, bottom in self.bottoms.items():
                if cv.pointPolygonTest(bottom, mid, False) > 0:
                    possible_bottoms.append(i)
            if not possible_bottoms:
                continue
            chosen_bottom = min(possible_bottoms, key=lambda i: cv.contourArea(self.bottoms[i]))
            symbols[chosen_bottom].append(symbol)
        return symbols

    def drop_smaller_symbols(self):
        """
        Removes symbols whose area is smaller than half of the maximum symbol area in the bottom.
        """
        for bottom_i, symbols in self.symbols_per_bottom.iteritems():
            if not symbols:
                continue
            threshold = max(map(cv.contourArea, symbols)) / 2
            self.symbols_per_bottom[bottom_i] = filter(lambda x: cv.contourArea(x) > threshold, symbols)

    def expected_area_from_length(self, length):
        """
        Computes an area that a card rectangle would have for this length.
        :param length: rectangle's length.
        :return: the expected rectangle's area.
        """
        x = length / (2 * 2.73)
        return x * x * 1.73

    def is_bottom(self, contour):
        area = cv.contourArea(contour)
        moments = cv.HuMoments(cv.moments(contour)).squeeze().tolist()
        if area < 10000:
            return False
        if not 4e-9 < abs(moments[2]) < 4e-6:
            return False
        if not 3e-10 < abs(moments[3]) < 1e-7:
            return False
        return True

    def is_potentially_card_symbol(self, contour):
        return 50 < cv.contourArea(contour) < 10000 and self.is_in_symbol_range(contour) and not self.is_white(contour)

    def is_in_symbol_range(self, contour):
        moments = cv.HuMoments(cv.moments(contour)).squeeze().tolist()
        if not 4e-8 < abs(moments[1]) < 8e-3:
            return False
        if not 4e-8 < abs(moments[2]) < 1e-2:
            return False
        return True

    @Slot()
    def find_contours(self):
        tmp_gray = cv.cvtColor(self.image, cv.COLOR_BGR2GRAY)
        self.contours, self.hierarchy = cv.findContours(tmp_gray, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)
        self.contours = self.filter_out_if_no_children(self.contours, self.hierarchy)
        cv.drawContours(self.image, self.contours, -1, (255, 0, 0), 2)
        self.image_changed.emit()

    def filter_out_if_no_children(self, contours, hierarchy):
        return [c for i, c in enumerate(contours) if hierarchy[0][i][2] != -1]

    def draw_bottom_contours(self):
        cv.drawContours(self.image, self.bottoms.values(), -1, (0, 255, 0), 2)

    @Slot()
    def find_bottoms(self):
        self.bottoms = {}
        for i, c in enumerate(filter(self.is_bottom, self.contours)):
            self.bottoms[i] = c
        self.draw_bottom_contours()
        self.image_changed.emit()

    @Slot()
    def find_potential_symbols(self):
        undistributed_symbols = filter(self.is_potentially_card_symbol, self.contours)
        symbols_per_bottom = self.distribute_symbols(undistributed_symbols)
        self.symbols_with_classes_per_bottom = self.fill_with_class(symbols_per_bottom, UNKNOWN)
        self.draw_symbol_contours()
        self.image_changed.emit()

    def draw_symbol_contours(self):
        for symbols in self.symbols_with_classes_per_bottom.values():
            for contour, cls in symbols:
                cv.drawContours(self.image, [contour], -1, self.color_for_class[cls], 2)

    def bgr_to_hsv(self, bgr):
        h, s, v = colorsys.rgb_to_hsv(*map(lambda x: x / 255.0, bgr[::-1]))
        return int(h * 360), int(s * 100), int(v * 100)

    def is_black(self, contour):
        symbol_color = self.original_image[self.contour_middle(contour)[::-1]]
        h, s, v = self.bgr_to_hsv(symbol_color)
        return (s + v) < 100

    def is_red(self, contour):
        symbol_color = self.original_image[self.contour_middle(contour)[::-1]]
        h, s, v = self.bgr_to_hsv(symbol_color)
        return (s + v) > 100 and (h < 5 or h > 345)

    def is_white(self, contour):
        symbol_color = self.original_image[self.contour_middle(contour)[::-1]]
        h, s, v = self.bgr_to_hsv(symbol_color)
        return v > 85 and s < 15

    def is_spades(self, moments):
        if abs(moments[3]) > 1e-6:
            return False
        if not 1e-13 < abs(moments[5]) < 1e-8:
            return False
        return True

    def is_clubs(self, moments):
        if not 1e-6 < abs(moments[3]) < 1e-4:
            return False
        if not 1e-8 < abs(moments[5]) < 1e-6:
            return False
        return True

    def is_hearts(self, moments):
        if abs(moments[1]) > 8e-5:
            return False
        if abs(moments[2]) < 1e-4:
            return False
        return True

    def is_diamonds(self, moments):
        if abs(moments[1]) < 8e-5:
            return False
        if abs(moments[2]) > 1e-4:
            return False
        return True

    def classify_symbol(self, contour):
        moments = cv.HuMoments(cv.moments(contour)).squeeze().tolist()
        if self.is_black(contour):
            if self.is_spades(moments):
                return SPADES
            elif self.is_clubs(moments):
                return CLUBS
        elif self.is_red(contour):
            if self.is_hearts(moments):
                return HEARTS
            elif self.is_diamonds(moments):
                return DIAMONDS
        return UNKNOWN

    def classify_symbols(self, dict_to_classify):
        classified = {}
        for bottom, symbols_with_classes in dict_to_classify.items():
            if not symbols_with_classes:
                continue
            symbols, classes = zip(*symbols_with_classes)
            classified[bottom] = zip(symbols, map(self.classify_symbol, symbols))
        return classified

    def filter_out_unknowns(self, dict_to_filter):
        filtered = {}
        for bottom, symbols_with_classes in dict_to_filter.items():
            filtered[bottom] = filter(lambda sc: sc[1] != UNKNOWN, symbols_with_classes)
        return filtered

    @Slot()
    def find_symbols(self):
        self.symbols_with_classes_per_bottom = self.classify_symbols(self.symbols_with_classes_per_bottom)
        self.symbols_with_classes_per_bottom = self.filter_out_unknowns(self.symbols_with_classes_per_bottom)
        self.draw_symbol_contours()
        self.image_changed.emit()

    def filter_out_much_smaller(self, dict_to_filter):
        filtered = {}
        for bottom, symbols_with_classes in dict_to_filter.items():
            contours = zip(*symbols_with_classes)[0]
            threshold = max(map(cv.contourArea, contours)) / 2
            filtered[bottom] = filter(lambda sc: cv.contourArea(sc[0]) > threshold, symbols_with_classes)
        return filtered

    def filter_out_not_consistent(self, dict_to_filter):
        filtered = {}
        for bottom, symbols_with_classes in dict_to_filter.items():
            classes = zip(*symbols_with_classes)[1]
            if len(set(classes)) == 1:
                filtered[bottom] = symbols_with_classes
        return filtered

    @Slot()
    def filter_out_bad_contours(self):
        self.bottoms = self.filter_out_bottoms_without_symbols(self.bottoms)
        self.symbols_with_classes_per_bottom = self.filter_out_much_smaller(self.symbols_with_classes_per_bottom)
        self.symbols_with_classes_per_bottom = self.filter_out_not_consistent(self.symbols_with_classes_per_bottom)
        tmp_bottoms = self.bottoms
        self.bottoms = {}
        for bottom_i in tmp_bottoms:
            if bottom_i in self.symbols_with_classes_per_bottom:
                self.bottoms[bottom_i] = tmp_bottoms[bottom_i]
        self.draw_bottom_contours()
        self.draw_symbol_contours()
        self.image_changed.emit()

    def filter_out_bottoms_without_symbols(self, bottoms):
        filtered = {}
        for bottom_i in bottoms:
            if bottom_i in self.symbols_with_classes_per_bottom:
                filtered[bottom_i] = bottoms[bottom_i]
        return filtered

    @Slot()
    def draw_contours_on_original(self):
        self.image = self.original_image.copy()
        self.draw_bottom_contours()
        self.draw_symbol_contours()
        self.image_changed.emit()

    def fill_with_class(self, symbols_per_bottom, to_fill):
        with_classes = {}
        for bottom, symbols in symbols_per_bottom.items():
            with_classes[bottom] = zip(symbols, itertools.repeat(to_fill, len(symbols)))
        return with_classes

    def recognize(self):
        tmp_gray = cv.cvtColor(self.image, cv.COLOR_BGR2GRAY)
        contours, hierarchy = cv.findContours(tmp_gray, cv.RETR_LIST, cv.CHAIN_APPROX_NONE)

        undistributed_symbols = filter(self.is_potentially_card_symbol, contours)
        self.bottoms = filter(self.is_bottom, contours)
        self.symbols_per_bottom = self.distribute_symbols(undistributed_symbols)
        self.drop_smaller_symbols()

        cv.drawContours(self.image, self.bottoms, -1, (0, 255, 0), 2)
        for symbols in self.symbols_per_bottom.values():
            cv.drawContours(self.image, symbols, -1, (0, 0, 255), 2)

        self.image_changed.emit()
