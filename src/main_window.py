from PySide.QtGui import QMainWindow, QFileDialog, QPixmap
from PySide.QtCore import Signal, Slot
from main_window_ui import Ui_mainWindow


class MainWindow(QMainWindow):
    def __init__(self, model):
        QMainWindow.__init__(self)
        self.ui = Ui_mainWindow()
        self.ui.setupUi(self)
        self.model = model
        self.model.image_changed.connect(self.update_image)
        self.ui.actionOpen.triggered.connect(self.open_file)
        self.ui.actionSave.triggered.connect(self.save_file)
        self.ui.actionBinarize.triggered.connect(self.model.binarize)
        self.ui.actionBlur.triggered.connect(self.model.blur)
        self.ui.actionContours.triggered.connect(self.model.find_contours)
        self.ui.actionBottoms.triggered.connect(self.model.find_bottoms)
        self.ui.actionPotentialSymbols.triggered.connect(self.model.find_potential_symbols)
        self.ui.actionSymbols.triggered.connect(self.model.find_symbols)
        self.ui.actionFiltering.triggered.connect(self.model.filter_out_bad_contours)
        self.ui.actionResult.triggered.connect(self.model.draw_contours_on_original)

    @Slot()
    def update_image(self):
        self.ui.image.setPixmap(self.model.current_pixmap())
        self.ui.centralwidget.update()
        
    @Slot()
    def save_file(self):
        filename = QFileDialog.getSaveFileName(self, "Zapisz obrazek", "../wyniki/", "Pliki *.jpg")[0]
        if filename:
            if not filename.endswith(".jpg"):
                filename += ".jpg"
            self.model.write_file(str(filename))
            
        
    @Slot()
    def open_file(self):
        filename = QFileDialog.getOpenFileName(self, "Wczytaj obrazek", "../zdjecia", "Pliki *.jpg")[0]
        if filename:
            self.model.load_image(str(filename))

