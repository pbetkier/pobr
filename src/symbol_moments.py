import cv2 as cv
from matplotlib import pyplot
from matplotlib.font_manager import FontProperties
import json
import model

symbol_names = ["trefle", "piki", "karo", "kiery", "unwanted"]
SYMBOLS_PER_IMAGE = 8
symbol_moments_filename = "../momenty/wartosci"

bottom_filenames = ["2 karty", "z innymi obiektami", "symbole/P1030950_scaled", "symbole/8kier"]
bottom_moments_filename = "../momenty/spody_wartosci"

moments_plot_filename_prefix = "../momenty/wykres_"

def slice_dict(to_slice, keys):
    sliced = {}
    for key in keys:
        sliced[key] = to_slice[key]
    return sliced


def save_moments(moments, filename):
    with open(filename, "w") as f:
        json.dump(moments, f)


def compute_symbols_moments():
    im = model.ImageModel()
    all_moments = dict.fromkeys(symbol_names)
    for symbol_name in symbol_names:
        im.load_image("../zdjecia/symbole/" + symbol_name + ".jpg")
        im.binarize()
        im.blur()
        im.find_contours()
        symbols = filter(im.is_potentially_card_symbol, im.contours)
        print len(symbols)
        #assert len(symbols) == SYMBOLS_PER_IMAGE
        all_moments[symbol_name] = [cv.HuMoments(cv.moments(s)).squeeze().tolist() for s in symbols]
    return all_moments


def compute_bottom_moments():
    im = model.ImageModel()
    bottom_moments = dict.fromkeys(bottom_filenames)
    for bottom_filename in bottom_filenames:
        im.load_image("../zdjecia/" + bottom_filename + ".jpg")
        im.binarize()
        im.blur()
        im.find_contours()
        bottoms = filter(lambda c: cv.contourArea(c) > 10000, im.contours)
        bottom_moments[bottom_filename] = [cv.HuMoments(cv.moments(b)).squeeze().tolist() for b in bottoms]
    return bottom_moments


def save_moments_plot(moments_per_symbol, filename=None):
    pyplot.close()
    colors = iter(['b', 'r', 'g', 'y', 'm'])
    for symbol, all_moments in moments_per_symbol.items():
        color = next(colors)
        for i, moments in enumerate(all_moments):
            label = symbol if i == 0 else '_nolegend_'
            pyplot.scatter(range(len(moments)), map(abs, moments), c=color, marker='o', label=label)
    pyplot.xlabel("moment")
    pyplot.ylabel("abs(wartosc)")
    pyplot.yscale('log', nonposy='clip', suby=[2])
    pyplot.ylim(1e-16, 1)
    pyplot.xlim(-0.5, 6.5)
    pyplot.minorticks_on()
    pyplot.yticks([pow(10, i) for i in range(-16, 1)])
    pyplot.grid(b=True, which='both')
    pyplot.legend(prop=FontProperties(size='small'), loc=3)
    filename = filename if filename is not None else moments_plot_filename_prefix + '_'.join(moments_per_symbol.keys()) + ".png"
    pyplot.savefig(filename)


def save_symbols_moments():
    all_moments = compute_symbols_moments()
    save_moments(all_moments, symbol_moments_filename)

    save_moments_plot(all_moments)
    save_moments_plot(slice_dict(all_moments, ["trefle", "piki"]))
    save_moments_plot(slice_dict(all_moments, ["karo", "kiery"]))


def save_bottom_moments():
    bottom_moments = compute_bottom_moments()
    save_moments(bottom_moments, bottom_moments_filename)

    save_moments_plot(bottom_moments, moments_plot_filename_prefix + "karta")


save_symbols_moments()
# save_bottom_moments()
