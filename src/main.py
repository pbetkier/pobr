#!/usr/bin/python

import sys
from PySide.QtGui import QApplication
from model import ImageModel
from main_window import MainWindow

#
# def setConnections(window, proc):
#     proc.show_image.connect(window.show_image)
#     proc.set_step.connect(window.set_step)
#     proc.set_processed.connect(window.set_processed)
#     proc.no_read_file.connect(window.no_file)
#     proc.set_filled_checked.connect(window.set_filled_checked)
#     proc.set_contour_checked.connect(window.set_contour_checked)
#     window.read_file.connect(proc.read_file)
#     window.write_file.connect(proc.write_file)
#     window.ui.actionStep.triggered.connect(proc.do_step)
#     window.ui.actionProcess.triggered.connect(proc.process)
#     window.ui.actionContour_detection.triggered.connect(proc.set_contour_detection)
#     window.ui.actionFilled_detection.triggered.connect(proc.set_filled_detection)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    image_model = ImageModel()
    mainWindow = MainWindow(image_model)
    mainWindow.show()
    app.exec_()
    sys.exit()